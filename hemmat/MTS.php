<?php

/**
 * 
 * @author Alireza Abedini
 * @version 1.0
 * @copyright 2004
 */
class MTSConvertor
{
	/**
	* Store Jalali Year after convert
	* 
	* @var int
	*/
	var $sYear;
	/**
	* Store Jalali Month after convert
	* 
	* @var int
	*/
	var $sMonth;
	/**
	* Store Jalali Day after convert
	* 
	* @var int
	*/
	var $sDay;


    function MTSConvertor($DateParam)
    {
        $date = split('-', $DateParam);
        $D = $date[0];
        $M = $date[1];
        $Y = $date[2];
        if ($Y < 100)
        {
            $Y += 2000;
        } 
        return (MTS($Y, $M, $D));
    } 
    // --------------------------------------------------------------------
    function div($a, $b)
    {
        return (int) ($a / $b);
    } 
    // --------------------------------------------------------------------
    function MTS($year, $month, $day)
    {
        global $dayays_in_month;
        global $j_days_in_month;

        $mYear = $year-1600;
        $mMonth = $month-1;
        $mDay = $day-1;

        $g_day_no = 365 * $mYear + div($mYear + 3, 4) - div($mYear + 99, 100) + div($mYear + 399, 400);

        for ($i = 0; $i < $mMonth; ++$i)
        $g_day_no += $g_days_in_month[$i];
        if ($mMonth > 1 && (($mYear % 4 == 0 && $mYear % 100 != 0) || ($mYear % 400 == 0)))
            /* leap and after Feb */
            ++$g_day_no;
        $g_day_no += $mDay;

        $j_day_no = $g_day_no-79;

        $j_np = div($j_day_no, 12053);
        $j_day_no %= 12053;

        $jy = 979 + 33 * $j_np + 4 * div($j_day_no, 1461);

        $j_day_no %= 1461;

        if ($j_day_no >= 366)
        {
            $jy += div($j_day_no-1, 365);
            $j_day_no = ($j_day_no-1) % 365;
        } 

        for ($i = 0; $i < 11 && $j_day_no >= $j_days_in_month[$i]; ++$i)
        {
            $j_day_no -= $j_days_in_month[$i];
        } 
        $this->sMonth = $i + 1;
        $this->sDay = $j_day_no + 1;

        return array($this->sDay, $this->sMonth, $this->sYear);
    } 
} 

?>
