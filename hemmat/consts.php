<?php
/////////////////////////////////////// MAIN VARS - most of them are for database
$hemmatconn = 0;

define('MAIN_URL', 'http://localhost/hemmat/');
define('HOSTNAME_HEMMAT', 'localhost');
define('DATABASE_HEMMAT', 'hemmat');
define('USERNAME_HEMMAT', 'root');
define('PASSWORD_HEMMAT', '');
define('ARCHIVE_PAGE', 'http://localhost/hemmat/archive.php');
define('NSUMCHAR', 50);
define('NSUMCHAR_BIG', 500);
define('TMP_PIC_DIR', 'images/tmp/');
define('TMP_PIC', 'tmp.jpg');
define('FILES_DIR', 'images/b/files/');
define('NOTES_IN_PAGE', 15);
define('FIRST_NO', 5); // Number of records in the first page of each topic
?>