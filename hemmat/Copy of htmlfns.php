﻿<?php
//************************** HTML TOP
function html_top($t = '', $h = '')
{
  if (!$t)
    $t = 'سردار خيبر حاج ابراهيم همت';
/*  if (!$h)
    $h = $t;*/
  mydbconnect();
?>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="content-language" content="fa">
  <link href="main.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" type="image/x-icon" href="./images/i.ico" />
  <script type="text/javascript">
    _editor_url = './htmlarea3';
    _editor_lang = 'fa';
  </script>
  <script type="text/javascript" src="./j.js"></script>
  <script type="text/javascript" src="./htmlarea3/htmlarea.js"></script>
  <script type="text/javascript" src="./htmlarea3/dialog.js"></script>
  <script tyle="text/javascript" src="./htmlarea3/lang/fa.js"></script>
  <title><?php echo $t; ?></title>
</head>
<body bgcolor="#f0f0f0">
<a name="ptop"></a>
<table>
<tr>
<td>
<?php
banner($h);
?>
</td>
</tr>
<tr>
<td>
<?php
bodytop();
}
//************************** HTML TOP - end 

//************************** BANNER
function banner($h = '')
{
?>
<table id="banner" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td align="right" width="5">
	  <img src="<? echo MAIN_URL; ?>images/banner_right.png">
	</td>
    <td style="background-image:url(<? echo MAIN_URL; ?>images/banner_bg.png);
	    vertical-align: middle; background-repeat:repeat-x">
	  <!--<div style="float:right; position:absolute">
	    &nbsp;
	    <font color="#FFFFFF" size="3" face="Times">
		سيد شهيدان اهل قلم:</font>
	  </div><br />-->
	  &nbsp;
	  <a href="<? echo MAIN_URL; ?>">
	  <img src="images/h.gif" />
	  <!--
      <span style="font-size:30px; font-family:times; color:#ffffff">
	    <strong>&nbsp;&nbsp;&nbsp;اين سردار خيبر قلعه قلب مرا نيز فتح كرده است</strong>
	  </span>
	  -->
	  </a>
    </td>
    <td style="background-image:url(<? echo MAIN_URL; ?>images/banner_bg.png);
	    vertical-align: middle; background-repeat:repeat-x; text-align:left">
	  <br>
<!--      <span style="font-size:20px; font-family:times; color:#ffffff">
	    <strong><nobr>سردار خيبر حاج ابراهيم همت&nbsp;&nbsp;&nbsp;</nobr></strong>
	  </span>-->
    </td>
    <td align="left" width="5">
	  <img src="<? echo MAIN_URL; ?>images/banner_left.png">
	</td>
  </tr>
</table>
<?php
}
//************************** BANNER - end

//************************** BODY TOP
function bodytop()
{
?>
  <table width="100%" id="page_body">
    <tr>
	  <td width="15%">
        <?php bodyright(); ?>
	  </td>
      <td width="70%">
<?php
}
//************************** BODY TOP - end

//************************** BODY RIGHT
function bodyright()
{
  show_categories();
  echo "<hr>\n";
  show_managementmenu();
}
//************************** BODY RIGHT - end

//************************** SHOW CATEGORIES
function show_categories()
{
	?>
	<table class="menu" cellspacing="2">
		<tr>
			<th>&nbsp;</th>
			<th>موضوعات</th>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<a href="<?php echo MAIN_URL; ?>">
				<nobr>صفحه اصلي</nobr></a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<a href="<?php echo MAIN_URL; ?>nahjolbalagheh">
				<nobr>بازگشت به نهج‌البلاغه</nobr></a>
			</td>
		</tr>
		<?php
		$mcat = 'select id, name from categories';// a where a.pid=0';
		$mcat = mysql_query($mcat);
		$nmcat = mysql_num_rows($mcat);
		for ($i = 0; $i < $nmcat; $i++)
		{
			$mcat1 = mysql_fetch_object($mcat);
		?>
		<tr>
			<td>&nbsp;</td>
			<td>
				<a href="<? echo MAIN_URL; ?>?cc=<? echo $mcat1->id; ?>">
				<nobr><? echo $mcat1->name; ?></nobr></a>
			</td>
		</tr>
		<? } ?>
	</table>
	<?php
}
//************************** SHOW CATEGORIES - end

//************************** SHOW MANAGEMENT MENU
function show_managementmenu()
{
?>
  <table class="menu" cellspacing="2">
    <tr>
    <th>&nbsp;</th>
      <th>منوي مديريت</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <a href="<? echo MAIN_URL; ?>">صفحه اصلي</a>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <a href="categories.php">دسته بندي</a>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <a href="notes.php">مطالب</a>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <a href="authors.php">نويسندگان</a>
      </td>
    </tr>
  </table>
<?php
}
//************************** SHOW MANAGEMENT MENU - end

//************************** BODY LEFT
function bodyleft()
{
?>
  <table>
    <tr>
      <td>
        <img src="<? echo MAIN_URL; ?>images/hemmat.jpg">
      </td>
    </tr>
  </table>
  <hr>
  <table class="menu" style="font-size: 9px;" cellspacing="2">
   <!-- <tr>
      <th>&nbsp;</th><th>جستجو</th>
    </tr>-->
    <tr>
      <td colspan="2">
        <form name="search_form" method="post" action="search.php">
          <input type="text" name="search_text" size="9">
          <input type="submit" value="جستجو">
        </form>
      </td>
    </tr>
  </table>
  <hr>
<?php  
  lastnotes();
}
//************************** BODY LEFT - end

//************************** LAST NOTES
function lastnotes()
{
?>
  <table class="menu" style="font-size: 9px;" cellspacing="2">
    <tr>
	  <th>&nbsp;</th><th>آخرين نوشته‌ها</th>
	</tr>
    <?php
    $lastnotes = 'select id, title from notes '.
      'where publish = 1 order by ndate desc, id desc limit 10';
    $lastnotes = mysql_query($lastnotes);
    $nlastnotes = mysql_num_rows($lastnotes);
    for ($nl = 0; $nl < $nlastnotes; $nl++)
    {
      $alast = mysql_fetch_object($lastnotes);
	  echo "<tr><td colspan=\"2\">\n";
  	  echo '<b>&bull;</b> <a href="'.MAIN_URL.'index.php?id='.$alast->id.'">';
	  echo $alast->title.'</a>';
	  echo "</td></tr>\n";
    }
	?>
  </table>
<?php
}
//************************** LAST NOTES - end

//************************** BODY BOTTOM
function bodybottom()
{
?>
    </td> <!-- 70% -->
	<td width="15%">
	  <?php bodyleft(); ?>
	</td>
  </tr>
</table>
<?php
}
//************************** BODY BOTTOM - end

//************************** SET ETC
function set_etc($t, $n)
{
  if (strlen($t) > $n)
    return '...';
  else
    return '';
}
//************************** SET ETC - end

//************************** HTML BOTTOM
function html_bottom()
{
  bodybottom();
?>
    </td>
  </tr>
</table>
<div align="center">
<a href="#ptop"><img src="./images/up.gif"></a>
</div>
</body>
</html>
<?php
}
//************************** HTML BOTTOM - end

//************************** HTMLAREA1
function htmlarea1($id = 'TA')
{
?>
<script type="text/javascript" defer="1">
    HTMLArea.replace("<?php echo $id; ?>");
</script>
<?php
}
//************************** HTMLAREA1 - end

//************************** SHOW RECORD
function show_record($eachn, $full_show)
{
	echo '<table width="100%">'."\n";
	echo '<tr>'."\n";
	echo '<td style="border-bottom: dashed #999999 1px"><img src="'.
		MAIN_URL.'images/bluebox.jpg">'."\n";
	echo '&nbsp;<strong><a href="index.php?id='.
		$eachn->id.'"><font color=#000080>'.$eachn->title;'</font></a></strong></td>'."\n";
	$cc1 = 'select id, name from categories a where a.id='.$eachn->ccode;
	$cc1 = mysql_query($cc1);
	$cc1 = mysql_fetch_object($cc1);
	echo '<th width="5%">';
	echo '<a href="'.MAIN_URL.'?cc='.$cc1->id.'">'.$cc1->name.'</a>';
	echo '</th>'."\n";
	echo '</tr>'."\n";
	$note_pic = './images/note_images/'.$eachn->ccode.'/'.$eachn->id.'/0.jpg';
	if (file_exists($note_pic))
		$note_pic = '<img src="'.$note_pic.'" width="75px" align="right">';
	else
		$note_pic = '';
	if ($full_show == 1)
	{
		echo '<tr><td colspan="2">';
		echo $eachn->text;'</td></tr>';
	}
	else
	{
		echo '<tr>'."\n";
		if (strlen($eachn->summary) > 1)
		  $ttt = $eachn->summary;
		else
		  $ttt = substr($eachn->text, 0, NSUMCHAR_BIG).
		  	set_etc($eachn->text, NSUMCHAR_BIG);
		echo '<td colspan="2">'.$note_pic.$ttt."\n";
		echo '<div align="left"><a href="index.php?id='.$eachn->id.
			'">ادامه...'.'</a></div>'."\n";
		echo '</td>'."\n";
		echo '</tr>'."\n";
	}
	echo '<tr><td colspan=2>'."\n";
	$au = 'select name from authors a where a.id='.$eachn->author;
	$au = mysql_query($au);
	if (@mysql_num_rows($au))
	{
		$au = mysql_fetch_object($au);
		$au = $au->name;
	}
	else
		$au = 'نامشخص';
	echo '<br>نويسنده: '.$au.' | <span dir="ltr">'.$eachn->ndate.'</span>'."\n";
	echo '</td></tr>'."\n";
	echo '</table>'."\n";
	echo '<hr color="#006600">'."\n";
}
//************************** SHOW RECORD - end

?>