﻿// I18N constants

// LANG: "fa", ENCODING: UTF-8 
// Author: Morteza Zafari
// Email: Lost@LostLord.com


HTMLArea.I18N = {

	// the following should be the filename without .js extension
	// it will be used for automatically load plugin language.
	lang: "fa",

	tooltips: {
		bold:           "ضخيم",
		italic:         "خميده",
		underline:      "زير خط دار",
		strikethrough:  "خط خورده",
		subscript:      "زيرنويس",
		superscript:    "توان نويس",
		justifyleft:    "چپ چين",
		justifycenter:  "وسط چين",
		justifyright:   "راست چين",
		justifyfull:    "تراز چين",
		orderedlist:    "ليست عددي",
		unorderedlist:  "ليست",
		outdent:        "کاهش زبانه",
		indent:         "افزايش زبانه",
		forecolor:      "رنگ قلم",
		hilitecolor:    "رنگ زمينه",
		horizontalrule: "خط افقي",
		createlink:     "درج لينک",
		insertimage:    "درج/ويرايش عکس",
		inserttable:    "درج جدول",
		htmlmode:       "نمايش بصورت کد",
		popupeditor:    "ويرايشگر بزرگتر",
		about:          "درباره اين ويرايشگر",
		showhelp:       "راهنماي استفاده",
		textindicator:  "قالب فعلي",
		undo:           "Undo",
		redo:           "Redo",
		cut:            "برش",
		copy:           "کپي",
		paste:          "Paste",
		lefttoright:    "چپ نويس",
		righttoleft:    "راست نويس"
	},

	buttons: {
		"ok":           "تاييد",
		"cancel":       "لغو"
	},

	msg: {
		"Path":         "مسير",
		"TEXT_MODE":    "شما در حالت متن قرار داريد.  از دکمه [<>] جهت بازگشت به حالت ويرايش استفاده کنيد",
		"IE-sucks-full-screen" :
		// translate here
		"The full screen mode is known to cause problems with Internet Explorer, " +
		"due to browser bugs that we weren't able to workaround.  You might experience garbage " +
		"display, lack of editor functions and/or random browser crashes.  If your system is Windows 9x " +
		"it's very likely that you'll get a 'General Protection Fault' and need to reboot.\n\n" +
		"You have been warned.  Please press OK if you still want to try the full screen editor."
	},

	dialogs: {
		"لغو"                                            : "Cancel",
		"درج / ويرايش لينک"                                : "Insert/Modify Link",
		"پنجره جديد (_blank)"                               : "New window (_blank)",
		"بدون تغيير (use implicit)"                               : "None (use implicit)",
		"تاييد"                                                : "OK",
		"ديگر"                                             : "Other",
		"همان فريم (_self)"     	                           : "Same frame (_self)",
		"Target:"                                           : "Target:",
		"عنوان (tooltip):"                                  : "Title (tooltip):",
		"فريم بالايي (_top)"                                  : "Top frame (_top)",
		"URL:"                                              : "URL:",
		"You must enter the URL where this link points to"  : "You must enter the URL where this link points to"
	}
};
