﻿<?php
//************************** MYDBCONNECT
function mydbconnect()
{
	global $hemmatconn;

	$hemmatconn = @mysql_pconnect(HOSTNAME_HEMMAT, USERNAME_HEMMAT, PASSWORD_HEMMAT);
	if (!$hemmatconn)
	die('خطا در اتصال به سرور بانک اطلاعاتی');
	if (!@mysql_select_db(DATABASE_HEMMAT))
	die('خطا در برقراری ارتباط با بانک اطلاعاتی');
}
//************************** MYDBCONNECT - end

//************************** GET CATEGORIES
function get_categories()//$pid = 0)
{
	$ccodes = 'select id, name from categories';// a where a.pid='.$pid;
	$ccodes = mysql_query($ccodes);
	$ccno = mysql_num_rows($ccodes);
	return array($ccno, $ccodes);
}
//************************** GET CATEGORIES - end

//************************** GET CATEGORY
function get_category($id = 0)
{
	$a_category = 'select id, name from categories a where a.id='.$id;
	$a_category = mysql_query($a_category);
	$a_n = mysql_num_rows($a_category);
	if (!$a_n)
	return 0;
	else
	{
		$a_category = mysql_fetch_object($a_category);
		return $a_category->name;
	}
}
//************************** GET CATEGORY - end

//************************** GET NUM OF RECORDS
function get_num_of_records($cc = '', $publish = 1)
{
	if (($cc == '') || (!$cc))
	$ccfilter = '';
	else
	$ccfilter = ' and a.ccode='.$cc;
	$table = 'notes';
	$q = 'select id from '.$table.' a where a.publish='.$publish.$ccfilter;
	$q = mysql_query($q);
	return mysql_num_rows($q);
}
//************************** GET NUM OF RECORDS - end

//************************** GET CHILD CATEGORIES
/*function get_child_categories($pid = 0, $e_pid = 0, $new = 1)
{
$child_categories = 'select id, name from categories a where a.pid='.$pid;
$child_categories = mysql_query($child_categories);
$children_no = mysql_num_rows($child_categories);
if ($children_no > 0)
{
global $spaces;
$spaces .= '&nbsp;';
for ($i = 0; $i < $children_no; $i++)
{
$child_category = mysql_fetch_object($child_categories);
if (!$new)
{
if ($cc1->id == $e_pid)
$selected = ' selected';
else
$selected = '';
}
echo '<option value="'.$child_category->id.'"'.$selected.'>'.
$spaces.$child_category->name.'</option>'."\n";
get_child_categories($child_category->id, $e_pid, 0);
}
$spaces = substr($spaces, 0, strlen($spaces) - 6);
}
return;
}*/
//************************** GET CHILD CATEGORIES - end

//************************** SHAMSY DATE
function MTSConvertor($DateParam)
{
	$date = split('-', $DateParam);
	$D = $date[2];
	$M = $date[1];
	$Y = $date[0];
	if ($Y < 100)
	{
		$Y += 2000;
	}
	return (MTS($Y, $M, $D));
}
// ---------------------------------
function div($a, $b)
{
	return (int) ($a / $b);
}
// ---------------------------------
function MTS($year, $month, $day)
{
//	global $dayays_in_month;
//	global $j_days_in_month;
	$j_days_in_month = array(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29);
	$g_days_in_month = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

	$mYear = $year-1600;
	$mMonth = $month-1;
	$mDay = $day-1;

	$g_day_no = 365 * $mYear + div($mYear + 3, 4) - div($mYear + 99, 100) + div($mYear + 399, 400);

	for ($i = 0; $i < $mMonth; ++$i)
	$g_day_no += $g_days_in_month[$i];
	if ($mMonth > 1 && (($mYear % 4 == 0 && $mYear % 100 != 0) || ($mYear % 400 == 0)))
	/* leap and after Feb */
	++$g_day_no;
	$g_day_no += $mDay;

	$j_day_no = $g_day_no-79;

	$j_np = div($j_day_no, 12053);
	$j_day_no %= 12053;

	$jy = 979 + 33 * $j_np + 4 * div($j_day_no, 1461);

	$j_day_no %= 1461;

	if ($j_day_no >= 366)
	{
		$jy += div($j_day_no-1, 365);
		$j_day_no = ($j_day_no-1) % 365;
	}

	for ($i = 0; $i < 11 && $j_day_no >= $j_days_in_month[$i]; ++$i)
	{
		$j_day_no -= $j_days_in_month[$i];
	}
	$sMonth = $i + 1;
	$sDay = $j_day_no + 1;
	$sYear = $jy;

	return array($sDay, $sMonth, $sYear);
}


//************************** MYDATE
function mydate()
{
	$d = MTSConvertor(date('Y-m-d'));
	return $d[2].'-'.$d[1].'-'.$d[0];
}
//************************** MYDATE - end

//************************** SERVER URL
function server_url()
{
	$proto = "http" .
	((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "") . "://";
	$server = isset($_SERVER['HTTP_HOST']) ?
	$_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
	return $proto . $server;
}
//************************** SERVER URL - end

//************************** REDIRECT RELATIVE
function redirect_rel($relative_url, $mymsg = '', $haveparam = 0)
{
	$url = server_url() . dirname($_SERVER['PHP_SELF']) . "/" . $relative_url;
	if ($haveparam == 1)
	$m = '&';
	else
	{
		if ($haveparam == 0)
		$m = '?';
	}
	$url .= $m.'mymsg='.$mymsg;
	if (!headers_sent())
	{
		header('Location: $url');
	}
	else
	{
		echo "<meta http-equiv=\"refresh\" content=\"0;url=$url\">\r\n";
	}
}
//************************** REDIRECT RELATIVE - end

//************************** UPDATE NOTE
function update_note($e_note_id)
{
	$table = 'notes';
	$query = 'select title from '.$table.' a where '.
	'a.id='.$e_note_id;
	$e_note = mysql_query($query);
	$num = mysql_num_rows($e_note);
	if ($num == 0)
	echo 'رکوردی با این شماره پیدا نشد.';
	else
	{
		$e_note_title = $_POST['note_title'];
		$e_note_summary = $_POST['note_summary'];
		$e_note_text = $_POST['note_text'];
		$e_note_ndate = $_POST['note_ndate'];
		//$e_note_author = $_POST['note_author'];
		$e_note_ccode = $_POST['note_ccode'];
		$e_note_publish = $_POST['note_publish'];
		
		if ($e_note_publish == 'on')
		{
			$e_note_publish = 1;
		}
		else
		{
			$e_note_publish = 0;
		}
		$query = 'update notes set '.
		'title="'.$e_note_title.'", text="'.$e_note_text.
		'", ccode='.$e_note_ccode.', publish='.$e_note_publish.
		', summary="'.$e_note_summary.'", ndate="'.$e_note_ndate.'"'.
		' where notes.id='.$e_note_id;
		mysql_query($query);
		$no = mysql_affected_rows();
		$mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.<br/>';
	}
	
	redirect_rel('notes.php', $mymsg, 0);
}
//************************** UPDATE NOTE - end

?>